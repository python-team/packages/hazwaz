.. hazwaz documentation master file, created by
   sphinx-quickstart on Tue Feb 22 20:02:16 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

Documentation
-------------

The documentation for the latest development version of hazwaz can be
browsed online at https://hazwaz.trueelena.org; `PDF
<https://hazwaz.trueelena.org/hazwaz.pdf>`_ and `epub
<https://hazwaz.trueelena.org/hazwaz.epub>`_ versions are also
available [#onion]_.

.. [#onion] Everything is also available via onion, at
   http://3nywl5hdyt4rm7dzqmwu62segouffhx7jkcpajkwf3pnyme4noj5boad.onion/

The author can be contacted via email: webmaster AT trueelena DOT org.

.. only:: html

   Status Badges
   -------------

   Packaging
   ^^^^^^^^^

   .. image:: https://repology.org/badge/vertical-allrepos/hazwaz.svg
      :target: https://repology.org/project/hazwaz/versions

   CI
   ^^

   .. image:: https://builds.sr.ht/~valhalla/hazwaz.svg
      :target: https://builds.sr.ht/~valhalla/hazwaz

Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   testing
   reference/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
