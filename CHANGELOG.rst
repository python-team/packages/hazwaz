***********
 CHANGELOG
***********

Unreleased
==========

0.0.3
=====

* Make the package PEP561 compliant.
* Modernize packaging with pyproject.toml and setuptool_scm.

0.0.2
=====

* Add helper code for testing. See
  https://hazwaz.trueelena.org/testing.html
* Added optional support for `coloredlogs
  <https://coloredlogs.readthedocs.io/>`_ (if available it will be used
  by default unless explicitely disabled).

0.0.1
=====

* Initial release
