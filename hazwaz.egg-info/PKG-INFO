Metadata-Version: 2.1
Name: hazwaz
Version: 0.0.3
Summary: A command line scripts framework
Author-email: Elena ``of Valhalla'' Grandi <valhalla@trueelena.org>
License: AGPLv3+
Project-URL: Homepage, https://hazwaz.trueelena.org/
Project-URL: Documentation, https://hazwaz.trueelena.org/
Project-URL: Repository, https://git.sr.ht/~valhalla/hazwaz
Project-URL: Source, https://git.sr.ht/~valhalla/hazwaz
Project-URL: Issues, https://todo.sr.ht/~valhalla/hazwaz
Project-URL: Tracker, https://todo.sr.ht/~valhalla/hazwaz
Project-URL: Changelog, https://git.sr.ht/~valhalla/hazwaz/tree/master/item/CHANGELOG.rst
Keywords: cli
Classifier: Development Status :: 3 - Alpha
Classifier: Environment :: Console
Classifier: Intended Audience :: Developers
Classifier: License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)
Classifier: Operating System :: POSIX
Classifier: Programming Language :: Python :: 3 :: Only
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Topic :: Software Development :: User Interfaces
Requires-Python: >=3.9
Description-Content-Type: text/x-rst

===============================
 hazwaz - command line library
===============================

hazwaz is a python3 library to write command line scripts.

Contributing
------------

Hazwaz is `hosted on sourcehut  <https://sr.ht/~valhalla/hazwaz>`_:

* `bug tracker <https://todo.sr.ht/~valhalla/hazwaz>`_
* `git repository <https://sr.ht/~valhalla/hazwaz/sources>`_
* `CI <https://builds.sr.ht/~valhalla/hazwaz>`_

License
-------

Copyright (C) 2022 Elena Grandi

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
